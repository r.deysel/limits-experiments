// With multiple extensions, what happens when you hit the static rule limits?

import fs from "fs";

function generateRules(numberOfRulesToGenerate) {
  let rules = [];

  for (let i = 1; i <= numberOfRulesToGenerate; i++) {
    rules.push({
      id: i,
      action: {type: "block"},
      condition: { urlFilter: "abc_" + i }
    });

    if (rules.length === numberOfRulesToGenerate)
      break;
    }

  return rules;
}

function createExtension(id, numberOfRulesToGenerate) {
  let manifest = {
    "name": "Test Extension " + id,
    "version": "0.0.1",
    "description": `Extension ${id} with ${numberOfRulesToGenerate} rules`,
    "manifest_version": 3,
    "background": {
        "service_worker": "background.js"
    },
    "permissions": [
        "declarativeNetRequest"
    ],
    "host_permissions": [
        "<all_urls>"
    ],
    "declarative_net_request": {
        "rule_resources": [
            {
                "id": "1",
                "enabled": true,
                "path": "rules.json"
            }
        ]
    }
  };  
  
  let rules = generateRules(numberOfRulesToGenerate);

  fs.mkdirSync(`Extension${id}`, {recursive: true});
  fs.writeFileSync(`Extension${id}/background.js`, "", "utf-8");
  fs.writeFileSync(`Extension${id}/manifest.json`, JSON.stringify(manifest, null, 2), "utf-8");
  fs.writeFileSync(`Extension${id}/rules.json`, JSON.stringify(rules, null, 2), "utf-8");
}

let extensions = [
  {id:1, numberOfRulesToGenerate: 329900},
  {id:2, numberOfRulesToGenerate: 60000},  
  // {id:2, numberOfRulesToGenerate: 31000},  
  // {id:4, numberOfRulesToGenerate: 50000}
];

for (let extension of extensions) {
  createExtension(extension.id, extension.numberOfRulesToGenerate);
}
